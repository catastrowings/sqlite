%bcond_without check

%global extver 3240000
%global tcl_version 8.6
%global tcl_sitearch %{_libdir}/tcl%{tcl_version}

Name:    sqlite
Version: 3.24.0
Release: 8
Summary: Embeded SQL database
License: Public Domain
URL:     http://www.sqlite.org/

Source0: http://www.sqlite.org/2018/sqlite-src-%{extver}.zip
Source1: http://www.sqlite.org/2018/sqlite-doc-%{extver}.zip
Source2: https://www.sqlite.org/2018/sqlite-autoconf-%{extver}.tar.gz

Patch0000: 0000-sqlite-no-malloc-usable-size.patch
Patch0001: 0001-sqlite-CVE-2018-20346.patch
Patch0002: 0002-remove-fail-testcase-in-no-free-fd-situation.patch

Patch6000: 6000-Fix-the-sqlite3BeginTrans-calls-within-the-snapshot-.patch
Patch6001: 6001-Change-a-comma-into-a-logically-equivalent-but-seman.patch
Patch6002: 6002-Fix-a-typo-in-the-amalgamation-autoconf-file.patch
Patch6003: 6003-Fix-typo-in-the-normalize-extension.patch
Patch6004: 6004-Fix-a-minor-problem-in-the-code-for-determining-whet.patch
Patch6005: 6005-Quick-patch-to-the-Lemon-parser-template-to-avoid-an.patch
Patch6006: 6006-Fix-typo-in-the-Win32-specific-code-for-the-fileio-e.patch
Patch6007: 6007-Fix-a-problem-causing-ENABLE_CURSOR_HINTS-builds-to-.patch
Patch6008: 6008-Fix-a-potential-crash-that-can-occur-while-reading-a.patch
Patch6009: 6009-In-the-CLI-fix-a-file-descriptor-leak-following-OOM-.patch
Patch6010: 6010-Take-steps-to-avoid-a-potential-integer-overflow-in-.patch
Patch6011: 6011-Fix-minor-memory-leak-in-the-dbstat-extension-that-c.patch
Patch6012: 6012-Fix-a-failing-assert-in-sqlite3ResetAllSchemasOfConn.patch
Patch6013: 6013-Fix-a-parser-bug-in-the-use-of-parentheses-around-ta.patch
Patch6014: 6014-Fix-possible-integer-overflow-while-running-PRAGMA-i.patch
Patch6015: 6015-Fix-a-segfault-caused-by-using-the-RAISE-function-in.patch
Patch6016: 6016-Fix-another-problem-with-corrupt-database-handling-i.patch
Patch6017: 6017-Fix-a-buffer-overwrite-in-fts5-triggered-by-a-corrup.patch
Patch6018: 6018-Fix-another-case-in-fts5-where-a-corrupt-database-co.patch
Patch6019: 6019-Fix-another-potential-buffer-overread-in-fts5.patch
Patch6020: 6020-Fix-a-possible-memory-leak-when-trying-to-UPDATE-a-c.patch
Patch6021: 6021-Fix-an-out-of-bounds-read-in-SQL-function-fts5_decod.patch
Patch6022: 6022-Fix-a-segfault-in-fts3-prompted-by-a-corrupted-datab.patch
Patch6023: 6023-Prevent-unsigned-32-bit-integer-overflow-from-leadin.patch
Patch6024: 6024-Fix-a-problem-causing-a-crash-if-an-fts5vocab-table-.patch
Patch6025: 6025-Fix-a-harmless-memory-leak-in-the-Lemon-parser-gener.patch
Patch6026: 6026-Handle-SQL-NULL-values-without-crashing-in-the-fts5-.patch
Patch6027: 6027-Fix-a-memory-leak-that-could-occur-in-fts3-when-hand.patch
Patch6028: 6028-Fix-a-buffer-overwrite-that-could-occur-when-running.patch
Patch6029: 6029-Fix-another-corruption-related-crash-in-fts5.patch
Patch6030: 6030-Avoid-integer-overflow-when-computing-the-array-of-a.patch
Patch6031: 6031-Fix-another-segfault-caused-by-a-corrupt-fts3-databa.patch
Patch6032: 6032-Fix-a-buffer-overrun-that-could-occur-in-fts5-if-a-p.patch
Patch6033: 6033-Fix-another-fts5-crash-that-can-occur-if-the-databas.patch
Patch6034: 6034-Fix-an-assert-in-vdbemem.c-that-could-fire-if-the-da.patch
Patch6035: 6035-Fix-a-potential-problem-with-INSERT-INTO-.-SELECT-FR.patch
Patch6036: 6036-Fix-a-segfault-that-could-follow-an-OOM-when-queryin.patch
Patch6037: 6037-Fix-a-buffer-overread-in-fts3-that-could-occur-when-.patch
Patch6038: 6038-Fix-a-buffer-overrun-triggered-by-a-merge-operation-.patch
Patch6039: 6039-Fix-another-buffer-overread-in-fts5-that-may-occur-w.patch
Patch6040: 6040-Fix-another-buffer-overrun-that-could-occur-when-que.patch
Patch6041: 6041-Fix-another-segfault-that-could-occur-in-fts5-with-a.patch
Patch6042: 6042-Fix-a-potential-memory-leak-in-RBU-if-the-rbu_fossil.patch
Patch6043: 6043-Fix-a-potential-32-bit-integer-overflow-in-the-showd.patch
Patch6044: 6044-sqlite-CVE-2019-8457-out-of-bounds-read.patch
Patch6045: 6045-sqlite-CVE-2019-16168.patch
Patch6046: 6046-Fix-CVE-2019-19646.patch
Patch6047: 6047-Fix-CVE-2019-9936.patch
Patch6048: 6048-Fix-CVE-2019-9937.patch
Patch6049: 6049-Fix-CVE-2019-19923-Continue-to-back-away-from-the-LEFT-JOIN-optimizatio.patch
Patch6050: 6050-Fix-CVE-2019-19924-When-an-error-occurs-while-rewriting-the-parser-tree.patch
Patch6051: 6051-Fix-CVE-2019-19925-Fix-the-zipfile-extension-so-that-INSERT-works-even-.patch
Patch6052: 6052-Fix-CVE-2019-19926-Continuation-of-e2bddcd4c55ba3cb-Add-another-spot-wh.patch
Patch6053: 6053-Fix-CVE-2019-20218-Do-not-attempt-to-unwind-the-WITH-stack-in-the-Parse.patch
Patch6054: 6054-Fix-the-zipfile-function-in-the-zipfile-extension-so.patch

BuildRequires: gcc autoconf tcl tcl-devel
BuildRequires: ncurses-devel readline-devel glibc-devel


Provides:  %{name}-libs
Obsoletes: %{name}-libs
Provides:  lemon
Obsoletes: lemon
Provides:  %{name}-analyzer
Obsoletes: %{name}-analyzer
Provides:  %{name}-tcl
Obsoletes: %{name}-tcl

%description
SQLite is a C-language library that implements a small, fast, self-contained,
high-reliability, full-featured, SQL database engine. SQLite is the most used
database engine in the world. SQLite is built into all mobile phones and most
computers and comes bundled inside countless other applications that people
use every day.It also include lemon and sqlite3_analyzer and tcl tools.

%package devel
Summary:  Including header files and library for the developing of sqlite
Requires: %{name}%{?_isa} = %{version}-%{release}
Requires: pkgconfig

%description devel
This contains dynamic libraries and header files for the developing of sqlite.

%package help
Summary:   Man file and documentation for sqlite
BuildArch: noarch
Provides:  %{name}-doc
Obsoletes: %{name}-doc

%description help
This contains man files and HTML files for the using of sqlite.


%prep
#autosetup will fail because of 2 zip files
%setup -q -a1 -n %{name}-src-%{extver}
%patch0000 -p1
%patch0001 -p0
%patch0002 -p1
%patch6000 -p1
%patch6001 -p1
%patch6002 -p1
%patch6003 -p1
%patch6004 -p1
%patch6005 -p1
%patch6006 -p1
%patch6007 -p1
%patch6008 -p1
%patch6009 -p1
%patch6010 -p1
%patch6011 -p1
%patch6012 -p1
%patch6013 -p1
%patch6014 -p1
%patch6015 -p1
%patch6016 -p1
%patch6017 -p1
%patch6018 -p1
%patch6019 -p1
%patch6020 -p1
%patch6021 -p1
%patch6022 -p1
%patch6023 -p1
%patch6024 -p1
%patch6025 -p1
%patch6026 -p1
%patch6027 -p1
%patch6028 -p1
%patch6029 -p1
%patch6030 -p1
%patch6031 -p1
%patch6032 -p1
%patch6033 -p1
%patch6034 -p1
%patch6035 -p1
%patch6036 -p1
%patch6037 -p1
%patch6038 -p1
%patch6039 -p1
%patch6040 -p1
%patch6041 -p1
%patch6042 -p1
%patch6043 -p1
%patch6044 -p1
%patch6045 -p1
%patch6046 -p1
%patch6047 -p1
%patch6048 -p1
%patch6049 -p1
%patch6050 -p1
%patch6051 -p1
%patch6052 -p1
%patch6053 -p1
%patch6054 -p1

rm -f %{name}-doc-%{extver}/sqlite.css~ || :

autoconf

%build
export CFLAGS="$RPM_OPT_FLAGS $RPM_LD_FLAGS -DSQLITE_ENABLE_COLUMN_METADATA=1 \
               -DSQLITE_DISABLE_DIRSYNC=1 -DSQLITE_ENABLE_FTS3=3 \
               -DSQLITE_ENABLE_RTREE=1 -DSQLITE_SECURE_DELETE=1 \
               -DSQLITE_ENABLE_UNLOCK_NOTIFY=1 -DSQLITE_ENABLE_DBSTAT_VTAB=1 \
               -DSQLITE_ENABLE_FTS3_PARENTHESIS=1 -DSQLITE_ENABLE_JSON1=1 \
               -Wall -fno-strict-aliasing"

%configure --enable-fts5 \
           --enable-threadsafe \
           --enable-threads-override-locks \
           --enable-load-extension \
           TCLLIBDIR=%{tcl_sitearch}/sqlite3

# rpath removal
sed -i 's|^hardcode_libdir_flag_spec=.*|hardcode_libdir_flag_spec=""|g' libtool
sed -i 's|^runpath_var=LD_RUN_PATH|runpath_var=DIE_RPATH_DIE|g' libtool

%make_build

%make_build sqlite3_analyzer

%install
make DESTDIR=${RPM_BUILD_ROOT} install

install -D -m 755 lemon %{buildroot}%{_bindir}/lemon
install -D -m 644 tool/lempar.c %{buildroot}%{_datadir}/lemon/lempar.c
install -D -m 644 sqlite3.1 %{buildroot}%{_mandir}/man1/sqlite3.1
install -D -m 755 sqlite3_analyzer %{buildroot}%{_bindir}/sqlite3_analyzer
chmod 755 %{buildroot}/%{tcl_sitearch}/sqlite3/*.so


%if %{with check}
%check
export LD_LIBRARY_PATH=`pwd`/.libs
export MALLOC_CHECK_=3

%ifarch x86_64 %{ix86}
%else
rm test/csv01.test
%endif

make test
%endif # with check

%ldconfig_scriptlets

%files
%doc README.md
%{_bindir}/{sqlite3,lemon,sqlite3_analyzer}
%{_libdir}/*.so.*
%{_datadir}/lemon
%{tcl_sitearch}/sqlite3
%exclude %{_libdir}/*.{la,a}

%files devel
%{_includedir}/*.h
%{_libdir}/*.so
%{_libdir}/pkgconfig/*.pc

%files help
%doc %{name}-doc-%{extver}/*
%{_mandir}/man*/*

%changelog
* Wed Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.24.0-8
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:CVE-2019-19959 fixed

* Wed Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.24.0-7
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:Fix CVE-2019-19923 CVE-2019-19924 CVE-2019-19925 CVE-2019-19926 CVE-2019-20218

* Fri Jan 3 2020 openEuler Buildteam <buildteam@openeuler.org> - 3.24.0-6
- Type:cves
- ID:CVE-2019-9936,CVE-2019-9937
- SUG:NA
- DESC:fix cve

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.24.0-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:fix CVE bug

* Wed Sep 11 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.24.0-4
- Type:enhancemnet
- ID:NA
- SUG:NA
- DESC:remove debuginfo

* Wed Aug 28 2019 openEuler Buildteam <buildteam@openeuler.org> - 3.24.0-3
- Type:enhancemnet
- ID:NA
- SUG:restart
- DESC:openEuler Debranding

* Tue Aug 20 2019 wubo<wubo40@huawei.com> - 3.24.0-2.h4
- Type:
- ID:
- SUG:NA
- DESC:rename patch

* Thu Aug 15 2019 wubo<wubo40@huawei.com> - 3.24.0-2.h3
- Type:bugfix
- ID:CVE-2019-8457
- SUG:NA
- DESC:heap out-of-bound read in function rtreenode()

* Sat Mar 23 2019 Xiaoqi Guo<guoxiaoqi2@huawei.com> - 3.24.0-2.h2
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:backport patch from community

* Sat Mar 23 2019 luochunsheng<luochunsheng@huawei.com> - 3.24.0-2.h1
- Type:cves
- ID:CVE-2018-20346
- SUG:NA
- DESC:CVE-2018-20346
- Package init
